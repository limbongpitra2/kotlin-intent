package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = "Google Pixel"

        val btnMove : Button = findViewById(R.id.move_act)
        btnMove.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
        when (v?.id){
            R.id.move_act->{
                val moveInt = Intent(this@MainActivity, MoveAct::class.java)
                startActivity(moveInt)
            }
        }
    }
}
